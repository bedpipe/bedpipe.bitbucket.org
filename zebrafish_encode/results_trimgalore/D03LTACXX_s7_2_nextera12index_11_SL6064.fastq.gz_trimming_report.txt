
SUMMARISING RUN PARAMETERS
==========================
Input filename: /gpfs/gpfs1/myerslab/data/Libraries/SL6064/D03LTACXX_s7_2_nextera12index_11_SL6064.fastq.gz
Trimming mode: paired-end
Trim Galore version: 0.4.1
Cutadapt version: 1.3.1-rc1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Adapter sequence: 'AGATCGGAAGAGC' (Illumina TruSeq, Sanger iPCR; user defined)
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length for both reads before a sequence pair gets removed: 20 bp
Output file will be GZIP compressed


cutadapt version 1.3.1-rc1
Command line parameters: -f fastq -e 0.1 -q 20 -O 1 -a AGATCGGAAGAGC /gpfs/gpfs1/myerslab/data/Libraries/SL6064/D03LTACXX_s7_2_nextera12index_11_SL6064.fastq.gz
Maximum error rate: 10.00%
   No. of adapters: 1
   Processed reads:     28637756
   Processed bases:   1431887800 bp (1431.9 Mbp)
     Trimmed reads:      9560906 (33.4%)
   Quality-trimmed:     69070136 bp (69.1 Mbp) (4.82% of total)
     Trimmed bases:     13626644 bp (13.6 Mbp) (0.95% of total)
   Too short reads:            0 (0.0% of processed reads)
    Too long reads:            0 (0.0% of processed reads)
        Total time:    485.39 s
     Time per read:      0.017 ms

=== Adapter 1 ===

Adapter 'AGATCGGAAGAGC', length 13, was trimmed 9560906 times.

No. of allowed errors:
0-9 bp: 0; 10-13 bp: 1

Overview of removed sequences
length	count	expect	max.err	error counts
1	6616517	7159439.0	0	6616517
2	2118028	1789859.8	0	2118028
3	643117	447464.9	0	643117
4	130734	111866.2	0	130734
5	41581	27966.6	0	41581
6	5882	6991.6	0	5882
7	1641	1747.9	0	1641
8	212	437.0	0	212
9	417	109.2	0	77 340
10	537	27.3	1	5 532
11	273	6.8	1	4 269
12	314	1.7	1	0 314
13	41	0.4	1	0 41
14	49	0.4	1	0 49
15	59	0.4	1	0 59
16	23	0.4	1	0 23
17	38	0.4	1	0 38
18	29	0.4	1	0 29
19	43	0.4	1	0 43
20	33	0.4	1	0 33
21	22	0.4	1	0 22
22	31	0.4	1	0 31
23	25	0.4	1	0 25
24	130	0.4	1	0 130
25	30	0.4	1	0 30
26	35	0.4	1	0 35
27	38	0.4	1	0 38
28	20	0.4	1	0 20
29	156	0.4	1	0 156
30	24	0.4	1	0 24
31	66	0.4	1	0 66
32	53	0.4	1	0 53
33	67	0.4	1	0 67
34	137	0.4	1	0 137
35	18	0.4	1	0 18
36	149	0.4	1	0 149
37	10	0.4	1	0 10
38	11	0.4	1	0 11
39	25	0.4	1	0 25
40	17	0.4	1	0 17
41	86	0.4	1	0 86
42	31	0.4	1	0 31
43	54	0.4	1	0 54
44	12	0.4	1	0 12
45	20	0.4	1	0 20
46	16	0.4	1	0 16
47	1	0.4	1	0 1
48	20	0.4	1	0 20
49	5	0.4	1	0 5
50	29	0.4	1	0 29


RUN STATISTICS FOR INPUT FILE: /gpfs/gpfs1/myerslab/data/Libraries/SL6064/D03LTACXX_s7_2_nextera12index_11_SL6064.fastq.gz
=============================================
28637756 sequences processed in total

Total number of sequences analysed for the sequence pair length validation: 28637756

Number of sequence pairs removed because at least one read was shorter than the length cutoff (20 bp): 626773 (2.19%)
