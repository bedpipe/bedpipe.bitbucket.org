PASS	Basic Statistics	SRR2040581_2.fastq.gz
PASS	Per base sequence quality	SRR2040581_2.fastq.gz
PASS	Per sequence quality scores	SRR2040581_2.fastq.gz
FAIL	Per base sequence content	SRR2040581_2.fastq.gz
FAIL	Per base GC content	SRR2040581_2.fastq.gz
WARN	Per sequence GC content	SRR2040581_2.fastq.gz
PASS	Per base N content	SRR2040581_2.fastq.gz
WARN	Sequence Length Distribution	SRR2040581_2.fastq.gz
FAIL	Sequence Duplication Levels	SRR2040581_2.fastq.gz
PASS	Overrepresented sequences	SRR2040581_2.fastq.gz
WARN	Kmer Content	SRR2040581_2.fastq.gz
