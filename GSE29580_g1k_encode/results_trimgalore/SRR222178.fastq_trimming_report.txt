
SUMMARISING RUN PARAMETERS
==========================
Input filename: /gpfs/gpfs1/home/aalonso/article/colorectal_GSE29580/fastq/SRR222178.fastq
Trimming mode: single-end
Trim Galore version: 0.4.1
Cutadapt version: 1.3.1-rc1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Adapter sequence: 'AGATCGGAAGAGC' (Illumina TruSeq, Sanger iPCR; user defined)
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length before a sequence gets removed: 20 bp
Output file will be GZIP compressed


cutadapt version 1.3.1-rc1
Command line parameters: -f fastq -e 0.1 -q 20 -O 1 -a AGATCGGAAGAGC /gpfs/gpfs1/home/aalonso/article/colorectal_GSE29580/fastq/SRR222178.fastq
Maximum error rate: 10.00%
   No. of adapters: 1
   Processed reads:     11461875
   Processed bases:    745021875 bp (745.0 Mbp)
     Trimmed reads:      3759467 (32.8%)
   Quality-trimmed:    116777398 bp (116.8 Mbp) (15.67% of total)
     Trimmed bases:      8693058 bp (8.7 Mbp) (1.17% of total)
   Too short reads:            0 (0.0% of processed reads)
    Too long reads:            0 (0.0% of processed reads)
        Total time:    306.38 s
     Time per read:      0.027 ms

=== Adapter 1 ===

Adapter 'AGATCGGAAGAGC', length 13, was trimmed 3759467 times.

No. of allowed errors:
0-9 bp: 0; 10-13 bp: 1

Overview of removed sequences
length	count	expect	max.err	error counts
1	2509349	2865468.8	0	2509349
2	790200	716367.2	0	790200
3	182591	179091.8	0	182591
4	53119	44772.9	0	53119
5	20056	11193.2	0	20056
6	12454	2798.3	0	12454
7	12612	699.6	0	12612
8	7884	174.9	0	7884
9	13242	43.7	0	13077 165
10	12118	10.9	1	11522 596
11	12349	2.7	1	11856 493
12	10243	0.7	1	9867 376
13	6923	0.2	1	6652 271
14	10585	0.2	1	10114 471
15	16121	0.2	1	15434 687
16	3832	0.2	1	3626 206
17	8165	0.2	1	7861 304
18	5325	0.2	1	5094 231
19	6845	0.2	1	6571 274
20	5536	0.2	1	5314 222
21	3847	0.2	1	3641 206
22	4576	0.2	1	4353 223
23	4582	0.2	1	4287 295
24	4460	0.2	1	4222 238
25	2796	0.2	1	2352 444
26	5197	0.2	1	4693 504
27	3326	0.2	1	3003 323
28	2086	0.2	1	1814 272
29	1765	0.2	1	1495 270
30	1922	0.2	1	1627 295
31	2228	0.2	1	1984 244
32	2857	0.2	1	2580 277
33	1684	0.2	1	1478 206
34	1646	0.2	1	1455 191
35	756	0.2	1	568 188
36	659	0.2	1	441 218
37	609	0.2	1	355 254
38	328	0.2	1	135 193
39	385	0.2	1	125 260
40	356	0.2	1	66 290
41	369	0.2	1	91 278
42	338	0.2	1	49 289
43	322	0.2	1	38 284
44	339	0.2	1	27 312
45	387	0.2	1	21 366
46	381	0.2	1	13 368
47	323	0.2	1	12 311
48	348	0.2	1	3 345
49	396	0.2	1	5 391
50	357	0.2	1	7 350
51	449	0.2	1	1 448
52	529	0.2	1	0 529
53	539	0.2	1	5 534
54	562	0.2	1	6 556
55	645	0.2	1	0 645
56	823	0.2	1	1 822
57	989	0.2	1	0 989
58	1173	0.2	1	0 1173
59	815	0.2	1	0 815
60	818	0.2	1	0 818
61	938	0.2	1	0 938
62	905	0.2	1	0 905
63	623	0.2	1	0 623
64	311	0.2	1	1 310
65	174	0.2	1	0 174


RUN STATISTICS FOR INPUT FILE: /gpfs/gpfs1/home/aalonso/article/colorectal_GSE29580/fastq/SRR222178.fastq
=============================================
11461875 sequences processed in total
Sequences removed because they became shorter than the length cutoff of 20 bp:	126999 (1.1%)

