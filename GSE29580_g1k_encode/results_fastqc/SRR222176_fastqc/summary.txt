PASS	Basic Statistics	SRR222176.fastq.gz
PASS	Per base sequence quality	SRR222176.fastq.gz
PASS	Per sequence quality scores	SRR222176.fastq.gz
FAIL	Per base sequence content	SRR222176.fastq.gz
FAIL	Per base GC content	SRR222176.fastq.gz
PASS	Per sequence GC content	SRR222176.fastq.gz
PASS	Per base N content	SRR222176.fastq.gz
WARN	Sequence Length Distribution	SRR222176.fastq.gz
WARN	Sequence Duplication Levels	SRR222176.fastq.gz
PASS	Overrepresented sequences	SRR222176.fastq.gz
FAIL	Kmer Content	SRR222176.fastq.gz
